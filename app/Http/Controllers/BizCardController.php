<?php

namespace App\Http\Controllers;

use App\BizCardDetails;
use Illuminate\Http\Request;
use JeroenDesloovere\VCard\VCard;
//use Response;
use Illuminate\Http\Response;
class BizCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        header('Content-Type: text/x-vcard;charset=utf-8;');
        $id = $request->id;
        $data = BizCardDetails::find($id);
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");

        $vcard = new VCard();

        $firstname = $data->name;

        $vcard->addName($firstname);

        $vcard->addCompany('Vitabiotics Egypt');
        $vcard->addJobtitle($data->job_title);
//        $vcard->addRole('Data Protection Officer');
        $vcard->addEmail($data->email);
        $vcard->addPhoneNumber($data->mobile, 'TYPE=MOBILE');
        $vcard->addPhoneNumber('034628063', 'TYPE=WORK');
        $vcard->addPhoneNumber('034628066', 'TYPE=FAX');
        $vcard->addAddress('Manufacturing site, Laboratories and Scientific office:
                                Block No.1 Parts Nos. From 8 to 15
                                4th Industrial Zone - New Borg El Arab City - Alexandria,Egypt ');
//        $vcard->addLabel('street, worktown, workpostcode Belgium');
        $vcard->addURL('http://www.vitabiotics-egypt.com','TYPE=Website');
        $vcard->addURL('https://www.pinterest.com/vitabioticsegypt/','TYPE=Pintrest');
        $vcard->addURL('https://www.facebook.com/vitabioticsegypt/','TYPE=Facebook');
        $vcard->addURL('https://www.linkedin.com/in/vitabiotics-egypt-a9125186/','TYPE=Linkedin');
        $vcard->addURL('https://www.instagram.com/vitabiotics.egypt/','TYPE=Instagram');
        $vcard->addURL('https://twitter.com/VitabioticsEg','TYPE=Twitter');
        $vcard->addPhoto(asset('assets/employees_images/'.$data->image));


        $content = $vcard->getOutput();
        $response = new Response();
        $response->setContent($content);
        $response->setStatusCode(200);
        if( $iPhone ){
            $response->headers->set('Content-Type', 'text/vcard');
        }else{
            $response->headers->set('Content-Type', 'text/x-vcard');
        }

        $response->headers->set('Content-Disposition', 'attachment; filename="'.'contact'.'.vcf"');
        $response->headers->set('Content-Length', mb_strlen($content, 'utf-8'));
        return $response;

// return vcard as a string
//return $vcard->getOutput();

// return vcard as a download
//       return $vcard->download();
//        return redirect()->back();
//redirect()->back();
// save vcard on disk
//$vcard->setSavePath('/path/to/directory');
//$vcard->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details=BizCardDetails::find($id);
//        dd($details);
        return view('index',['details'=>$details]);
//        dd($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
