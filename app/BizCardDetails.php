<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BizCardDetails extends Model
{
    protected $table = 'biz_card_details';
    protected $fillable = ['id','name','job_title','email','mobile','created_at','updated_at'];

}
