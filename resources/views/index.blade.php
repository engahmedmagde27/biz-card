<?php
/**
 * Created by PhpStorm.
 * User: AMagde
 * Date: 10/6/2019
 * Time: 10:09 AM
 */?>
        <!DOCTYPE HTML>
<html lang="en">
<head>
    <title>{{$details->name}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">


    <!-- Font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700%7CAllura" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/brands.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/regular.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/solid.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/svg-with-js.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/v4-font-face.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/v4-shims.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/v5-font-face.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/all.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/brands.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/conflict-detection.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/conflict-detection.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/fontawesome.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/regular.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/solid.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/v4-shims.min.js">





    <!-- Stylesheets -->

    <link href="{{asset('assets/common-css/bootstrap.css')}}" rel="stylesheet">

    <link href="{{asset('assets/common-css/ionicons.css')}}" rel="stylesheet">

    <link href="{{asset('assets/common-css/fluidbox.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/01-cv-portfolio/css/styles.css')}}" rel="stylesheet">

    <link href="{{asset('assets/01-cv-portfolio/css/responsive.css')}}" rel="stylesheet">

</head>
<body>

<header>
    <div class="container" >

        <button style="font-size:24px; color: darkgray; position: fixed ;z-index: 1" onclick="myFunction();"><i class="fa-solid fa-circle-info fa-beat-fade"></i></button>
        <div class="heading-wrapper" id="hed" style="display: none; ">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="info">
                        <i class="icon ion-ios-location-outline"></i>
                        <div class="right-area">
                            {{--<h3>Vitabiotics Egypt for Pharmaceutical Industries (S.A.E)</h3>--}}
                            <h5>Manufacturing site, Laboratories and Scientific office:
                                Block No.1 Parts Nos. From 8 to 15
                                4th Industrial Zone - New Borg El Arab City - Alexandria,Egypt </h5>
                            {{--<h5>Alexandria</h5>--}}
                        </div><!-- right-area -->
                    </div><!-- info -->
                </div><!-- col-sm-4 -->

                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="info">
                        <i class="icon ion-ios-telephone-outline"></i>
                        <div class="right-area">
                            <h5><a href=""></a>Phone: (002 03) 4628063 - (002 03) 4628064 - (002 03) 4628065 </h5>
                            <h5>Fax: (002 03) 4628066</h5>
                            <h6>Su - TH,8AM - 4.30PM</h6>
                        </div><!-- right-area -->
                    </div><!-- info -->
                </div><!-- col-sm-4 -->

                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="info">
                        <i class="icon ion-ios-chatboxes-outline"></i>
                        <div class="right-area">
                            <h5>info@vitabiotics-egypt.com</h5>
                            <h6>REPLY IN 24 HOURS</h6>
                        </div><!-- right-area -->
                    </div><!-- info -->
                </div><!-- col-sm-4 -->
            </div><!-- row -->
        </div><!-- heading-wrapper -->
        {{--<i class="fa fa-address-book" aria-hidden="true"></i>--}}
        <a class="downlad-btn" href="{{route('getData',['id'=>$details->id])}}" style="border-radius: 100px"><i class="fa fa-user-plus fa-2xl fa-beat-fade" aria-hidden="true"></i></a>
        <br>
    </div><!-- container -->
</header>

<section class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-lg-2"></div>
            <div class="col-md-10 col-lg-8">
                <div class="intro">
                    <div class="profile-img"><img src="{{asset('assets/employees_images/'.$details->image)}}" alt="" style=""></div>
                    <h2><b>{{$details->name}}</b></h2>
                    <h4 class="font-yellow">{{$details->job_title}}</h4>
                    <ul class="information margin-tb-30">
                        {{--<li><b>BORN : </b>August 25, 1987</li>--}}
                        {{--<li><b>EMAIL : </b>mymith@mywebpage.com</li>--}}
                        {{--<li><b>MARITAL STATUS : </b>Married</li>--}}
                    </ul>
                    <ul class="social-icons">
                        <li><a href="https://www.pinterest.com/vitabioticsegypt/" target="_blank"><i class="ion-social-pinterest"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/vitabiotics-egypt-a9125186/" target="_blank"><i class="ion-social-linkedin"></i></a></li>
                        <li><a href="https://www.instagram.com/vitabiotics.egypt/" target="_blank"><i class="ion-social-instagram"></i></a></li>
                        <li><a href="https://www.facebook.com/vitabioticsegypt/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="https://twitter.com/VitabioticsEg" target="_blank"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="http://www.vitabiotics-egypt.com/" target="_blank"><i class="ion-android-globe"></i></a></li>
                        <li><a href="mailto:{{$details->email}}" target="_blank"><i class="ion-email-unread"></i></a></li>
                        <li><a href="tel:{{$details->mobile}}"><i class="ion-ios-telephone"></i></a></li>
                        <li><a href="https://goo.gl/maps/aWzBYMkPSo62wGoAA"><i class="ion-ios-location"></i></a></li>
                        <li><a href="{{route('getData',['id'=>$details->id])}}"><i class="ion-ios-contact"></i></a></li>

                    </ul>
                </div><!-- intro -->
            </div><!-- col-sm-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- intro-section -->





<footer>
    <p class="copyright">
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Vitabiotics Egypt <i class="ion-heart" aria-hidden="true"></i> by <a href="https://vitabiotics-egypt.com" target="_blank">Vitabiotics</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    </p>


</footer>


<!-- SCIPTS -->

<script src="{{asset('assets/common-js/jquery-3.2.1.min.js')}}"></script>

<script src="{{asset('assets/common-js/tether.min.js')}}"></script>

<script src="{{asset('assets/common-js/bootstrap.js')}}"></script>

<script src="{{asset('assets/common-js/isotope.pkgd.min.js')}}"></script>

<script src="{{asset('assets/common-js/jquery.waypoints.min.js')}}"></script>

<script src="{{asset('assets/common-js/progressbar.min.js')}}"></script>

<script src="{{asset('assets/common-js/jquery.fluidbox.min.js')}}"></script>

<script src="{{asset('assets/common-js/scripts.js')}}"></script>
<script>
    function myFunction() {
        var x = document.getElementById("hed");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>
</body>
</html>
